﻿using sip_waw.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.Entity.Spatial;

namespace sip_waw.TextFileParser
{
    public class TextFileParser
    {
        public string Filepath { get; set; } = @"C:\Users\wojtek\Desktop\RA180428 — kopia.TXT";

        public ApplicationDbContext AppDbContext { get; set; }

        public ParsedData ParsedDataLists { get; set; }

        public void ParseText()
        {
            ParsedDataLists = new ParsedData();

            using (StreamReader reader = new StreamReader(Filepath, Encoding.GetEncoding(1250)))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    string openingSection = line.Substring(0, 3);

                    if (openingSection == "###")
                        break;

                    int itemsCount = int.Parse(line.Substring(3).Replace(" ", string.Empty));
                    switch (openingSection)
                    {
                        case "*TY":
                            ParseDayTypes(reader, itemsCount);
                            break;
                        case "*KA":
                            ParseTimetableCalendarTypes(reader, itemsCount);
                            break;
                        case "*KD":
                            ParseTimetableCalendar(reader, itemsCount);
                            break;
                        case "*ZA":
                            ParseStopGroups(reader, itemsCount);
                            break;
                        case "*ZP":
                            ParseStopPosts(reader, itemsCount);
                            break;
                        case "*SM":
                            ParseTownCodes(reader, itemsCount);
                            break;
                        case "*LL":
                            ParseLineRoutes(reader, itemsCount);
                            break;
                    }

                }
            }
        }

        private void ParseDayTypes(StreamReader reader, int entriesCount)
        {
            for (int i = 0; i < entriesCount; i++)
            {
                string line = reader.ReadLine();

                string daytypeId = line.Substring(3, 2);
                string daytypeName = line.Substring(8);

                ParsedDataLists.DayTypesList.Add(new Entity.Entities.DayType() { Symbol = daytypeId, Description = daytypeName });
            }
            string closeSection = reader.ReadLine();
            if (closeSection.Replace(" ", string.Empty) != "#TY")
                throw new Exception("Error closing section");

        }

        private void ParseTimetableCalendarTypes(StreamReader reader, int entriesCount)
        {
            for (int i = 0; i < entriesCount; i++)
            {
                reader.ReadLine();
            }

            string closeSection = reader.ReadLine();
            if (closeSection.Replace(" ", string.Empty) != "#KA")
                throw new Exception("Error closing section KA");
        }

        private void ParseTimetableCalendar(StreamReader reader, int entriesCount)
        {
            for (int i = 0; i < entriesCount; i++)
            {
                string dateLine = reader.ReadLine();

                DateTime day = DateTime.Parse(dateLine.Substring(3, 10));
                int lineEntries = int.Parse(dateLine.Substring(15));

                for (int j = 0; j < lineEntries; j++)
                {
                    string lineEntry = reader.ReadLine();

                    string lineNumber = lineEntry.Substring(0, 11).Replace(" ", string.Empty);
                    string dayType = lineEntry.Substring(12, 2);

                    //AppDbContext.TimetableCalendars.Add(new Entity.Entities.TimetableCalendar() { Date = day, DayTypeSymbol = dayType, LineNumber = lineNumber });
                    ParsedDataLists.TimetableCalendarsList.Add(new Entity.Entities.TimetableCalendar() { Date = day, DayTypeSymbol = dayType, LineNumber = lineNumber });
                }
            }

            string closeSection = reader.ReadLine();
            if (closeSection.Replace(" ", string.Empty) != "#KD")
                throw new Exception("Error closing section KD");
        }

        private void ParseStopGroups(StreamReader reader, int entriesCount)
        {
            for (int i = 0; i < entriesCount; i++)
            {
                string stopGroupLine = reader.ReadLine();

                string number = stopGroupLine.Substring(3, 4);
                string name = stopGroupLine.Substring(10, 30);
                if (name.Contains(','))
                    name = name.Substring(0, name.LastIndexOf(','));

                string townCode = stopGroupLine.Substring(46, 2);

                //  AppDbContext.StopGroups.Add(new Entity.Entities.StopGroup() { Number = number, Name = name, TownCode = townCode });
                ParsedDataLists.StopGroupsList.Add(new Entity.Entities.StopGroup() { Number = number, Name = name, TownCode = townCode });
            }

            string closeSection = reader.ReadLine();
            if (closeSection.Replace(" ", string.Empty) != "#ZA")
                throw new Exception("Error closing section ZA");
        }

        private void ParseStopPosts(StreamReader reader, int entriesCount)
        {
            for (int i = 0; i < entriesCount; i++)
            {
                Console.WriteLine($"Zespół przystankowy nr {i + 1} // {entriesCount}");

                string line = reader.ReadLine();
                string groupNumber = line.Substring(3, 4);

                string openingSectionLine = reader.ReadLine();
                int postsAmount = int.Parse(openingSectionLine.Substring(9).Replace(" ", string.Empty));

                for (int j = 0; j < postsAmount; j++)
                {
                    string postString = reader.ReadLine();

                    string postNumber = postString.Substring(13, 2);
                    string streetName = postString.Substring(34, 30);
                    if (streetName.Contains(','))
                        streetName = streetName.Substring(0, streetName.LastIndexOf(','));

                    string commonDirection = postString.Substring(75);
                    if (commonDirection.Contains(','))
                        commonDirection = commonDirection.Substring(0, commonDirection.LastIndexOf(','));
                    else
                        commonDirection = commonDirection.Substring(0, 30);

                    string pozY = postString.Substring(112, 9);
                    string pozX = postString.Substring(129, 9);

                    DbGeography position;
                    //DO SPRAWDZENIA UKŁAD ODNIESIENIA, BO NIE MA PODANEGO!
                    try
                    {
                        position = DbGeography.PointFromText($"POINT({pozX} {pozY})", 4326);
                    }
                    catch
                    {
                        position = null;
                    }

                    int stoppingTypesCount = int.Parse(postString.Substring(18, 1));
                    for (int k = 0; k < stoppingTypesCount; k++)
                    {
                        string linesOnStopString = reader.ReadLine();
                        string stoppingCharacter = linesOnStopString.Substring(20, linesOnStopString.IndexOf(":") - 20);

                        Entity.Enums.StoppingCharacter character = Entity.Enums.StoppingCharacter.Stały;

                        switch (stoppingCharacter)
                        {
                            case "stały":
                                character = Entity.Enums.StoppingCharacter.Stały;
                                break;
                            case "na żądanie":
                                character = Entity.Enums.StoppingCharacter.Warunkowy;
                                break;
                            case "krańcowy":
                                character = Entity.Enums.StoppingCharacter.Krańcowy;
                                break;
                            case "dla wysiadających":
                                character = Entity.Enums.StoppingCharacter.Dla_wysiadających;
                                break;
                            case "dla wsiadających":
                                character = Entity.Enums.StoppingCharacter.Dla_wsiadających;
                                break;
                            case "postojowy":
                                character = Entity.Enums.StoppingCharacter.Postojowy;
                                break;
                            default:
                                throw new Exception("Unknown stopping type!");
                        }

                        int linesCount = int.Parse(linesOnStopString.Substring(13, 5));

                        for (int m = 0; m < linesCount; m++)
                        {
                            string lineNumber = linesOnStopString.Substring(40 + m * 6, 3).Replace(" ", string.Empty);

                            bool isPublished = linesOnStopString[40 + (6 * m) + 3] != '^';

                            //AppDbContext.StopPost_Lines.Add(new Entity.Entities.StopPost_Line()
                            //{
                            //    IsPublished = isPublished,
                            //    LineNumber = lineNumber,
                            //    PostGroupNumber = groupNumber,
                            //    StoppingCharacter = character,
                            //    PostNumber = postNumber
                            //});

                            ParsedDataLists.StopPost_LinesList.Add(new Entity.Entities.StopPost_Line()
                            {
                                IsPublished = isPublished,
                                LineNumber = lineNumber,
                                PostGroupNumber = groupNumber,
                                StoppingCharacter = character,
                                PostNumber = postNumber
                            });
                        }
                    }
                    //POTEM ZMIENIĆ GDY WCZYTAMY JUŻ NUMERY LINII
                    sip_waw.Entity.Enums.StopType stopType = Entity.Enums.StopType.Autobusowy;

                    //AppDbContext.StopPosts.Add(new Entity.Entities.StopPost()
                    //{
                    //    CommonDestination = commonDirection,
                    //    Position = position,
                    //    PostNumber = postNumber,
                    //    StopGroupNumber = groupNumber,
                    //    StreetName = streetName,
                    //    StopType = stopType
                    //});

                    ParsedDataLists.StopPostsList.Add(new Entity.Entities.StopPost()
                    {
                        CommonDestination = commonDirection,
                        Position = position,
                        PostNumber = postNumber,
                        StopGroupNumber = groupNumber,
                        StreetName = streetName,
                        StopType = stopType
                    });
                }
                string closesubSection = reader.ReadLine();
                if (closesubSection.Replace(" ", string.Empty) != "#PR")
                    throw new Exception("Error closing section PR");
            }

            string closeSection = reader.ReadLine();
            if (closeSection.Replace(" ", string.Empty) != "#ZP")
                throw new Exception("Error closing section ZP");
        }

        private void ParseTownCodes(StreamReader reader, int entriesCount)
        {
            for (int i = 0; i < entriesCount; i++)
            {
                string townLine = reader.ReadLine();

                string townCode = townLine.Substring(3, 2);
                string townName = townLine.Substring(8);

                //AppDbContext.Towns.Add(new Entity.Entities.Town() { Code = townCode, Name = townName });
                ParsedDataLists.TownsList.Add(new Entity.Entities.Town() { Code = townCode, Name = townName });
            }

            string closeSection = reader.ReadLine();
            if (closeSection.Replace(" ", string.Empty) != "#SM")
                throw new Exception("Error closing section SM");
        }

        private void ParseLineRoutes(StreamReader reader, int entriesCount)
        {
            for (int i = 0; i < entriesCount; i++)
            {
                string lineString = reader.ReadLine();
                string lineNumber = lineString.Substring(10, 5).Replace(" ", string.Empty);
                string lineDescription = lineString.Substring(17);

                Console.WriteLine($"Linia {i + 1} // {entriesCount} || Nr {lineNumber}");

                ParsedDataLists.LinesList.Add(new Entity.Entities.Line() { Number = lineNumber, Description = lineDescription });

                string routesSectionOpening = reader.ReadLine();
                int routeTypesCount = int.Parse(routesSectionOpening.Substring(9).Replace(" ", string.Empty));

                ParsedDataLists.DeparturesDictionary.Add(lineNumber, new List<Entity.Entities.Departure>());

                //wpisy sekcji TR - opis przebiegów tras
                for (int j = 0; j < routeTypesCount; j++)
                {
                    string routeSectionHeader = reader.ReadLine();
                    string routeName = routeSectionHeader.Substring(9, routeSectionHeader.IndexOf(',') - 9).Replace(" ", string.Empty);

                    //Console.WriteLine($"Przebieg trasy {routeName}   {j} // {routeTypesCount}");

                    Entity.Enums.RouteDirection routeDirection;
                    if (routeSectionHeader[113] == 'A')
                        routeDirection = Entity.Enums.RouteDirection.A;
                    else if (routeSectionHeader[113] == 'B')
                        routeDirection = Entity.Enums.RouteDirection.B;
                    else
                        throw new Exception("Unknown route direction!");

                    int routePoz = int.Parse(routeSectionHeader.Substring(122));

                    ParsedDataLists.RouteTypesList.Add(new Entity.Entities.RouteType()
                    {
                        LineNumber = lineNumber,
                        Name = routeName,
                        RouteDirection = routeDirection,
                        RouteLevel = routePoz
                    });

                    string routeStopsSubsectionOpening = reader.ReadLine();
                    int stopsCount = int.Parse(routeStopsSubsectionOpening.Substring(15).Replace(" ", string.Empty));

                    int ommitedLines = 0;
                    for (int k = 0; k < stopsCount; k++)
                    {

                        string routeStopString = reader.ReadLine();

                        string stopGroupNumber = routeStopString.Substring(49, 4);
                        string stopPostNumber = routeStopString.Substring(53, 2);

                        if (string.IsNullOrWhiteSpace(stopGroupNumber) || string.IsNullOrWhiteSpace(stopPostNumber))
                        {
                            ommitedLines++;
                            continue;
                        }


                        ParsedDataLists.StopPost_RouteTypesList.Add(new Entity.Entities.StopPost_RouteType()
                        {
                            LineNumber = lineNumber,
                            PostGroupNumber = stopGroupNumber,
                            PostNumber = stopPostNumber,
                            RouteTypeName = routeName,
                            StopNumber = k - ommitedLines
                        });
                    }
                    string closeStopsSubSection = reader.ReadLine();
                    if (closeStopsSubSection.Replace(" ", string.Empty) != "#LW")
                        throw new Exception("Error closing section LW");

                    string openTimetablesSection = reader.ReadLine();
                    stopsCount = int.Parse(openTimetablesSection.Substring(15).Replace(" ", string.Empty));

                    //int, int - godz/min, string string - oznaczenie/opis
                    for (int k = 0; k < stopsCount; k++)
                    {
                        List<Tuple<int, int, string, string>> departureLegend = new List<Tuple<int, int, string, string>>();

                        string stopData = reader.ReadLine();
                        string groupNumber = stopData.Substring(15, 4);
                        string postNumber = stopData.Substring(19, 2);

                        string dayTypesOpenSection = reader.ReadLine();
                        int dayTypesCount = int.Parse(dayTypesOpenSection.Substring(21).Replace(" ", string.Empty));
                        for (int m = 0; m < dayTypesCount; m++)
                        {
                            string dayTypeLine = reader.ReadLine();
                            string dayType = dayTypeLine.Substring(21, 2);

                            //TRZEBA BĘDZIE JAKOŚ UMIEŚCIĆ  INFORMACJĘ, ŻE DANA LINIA DANEGO DNIA NIE KURSUJE, BO TO JEST OBOWIĄZKOWE
                            if (dayTypeLine.Contains("NIE KURSUJE"))
                                continue;

                            int hoursCount = int.Parse(reader.ReadLine().Substring(27).Replace(" ", string.Empty));
                            for (int n = 0; n < hoursCount; n++)
                            {
                                string hourMinutesLine = reader.ReadLine();
                                int minutesCount = int.Parse(hourMinutesLine.Substring(29, 2).Replace(" ", string.Empty));

                                int hour = int.Parse(hourMinutesLine.Substring(33, 2).Replace(" ", string.Empty));

                                for (int o = 0; o < minutesCount; o++)
                                {
                                    string minutesString = hourMinutesLine.Substring(37 + (o * 6), 6);
                                    int minute = int.Parse(minutesString.Substring(2, 2));

                                    if (minutesString[1] == ' ')
                                        departureLegend.Add(new Tuple<int, int, string, string>(hour, minute, "_", dayType));

                                    if (minutesString[4] != ' ' && minutesString[4] != '^' && minutesString[4] != ']')
                                        departureLegend.Add(new Tuple<int, int, string, string>(hour, minute, minutesString[4].ToString(), dayType));

                                    if (minutesString[5] != ' ' && minutesString[5] != '^' && minutesString[5] != ']')
                                        departureLegend.Add(new Tuple<int, int, string, string>(hour, minute, minutesString[5].ToString(), dayType));
                                }
                            }
                            string closeMinutesHoursSubsection = reader.ReadLine();
                            if (closeMinutesHoursSubsection.Replace(" ", string.Empty) != "#WG")
                                throw new Exception("Error closing section WG");

                            int departuresEntriesCount = int.Parse(reader.ReadLine().Substring(27).Replace(" ", string.Empty));

                            for (int n = 0; n < departuresEntriesCount; n++)
                            {
                                //Console.WriteLine($"Odjazd  {n} // {departuresEntriesCount} || Nr {lineNumber} || Przebieg {routeName} :  {j} / {routeTypesCount}");

                                string departureLine = reader.ReadLine();
                                int hour = int.Parse(departureLine.Substring(27, 2).Replace(" ", string.Empty));
                                int minute = int.Parse(departureLine.Substring(30, 2));

                                string courseId = departureLine.Substring(34, 17);

                                bool isLowFloor = departureLegend.Find(x => (x.Item1 == hour) && (x.Item2 == minute) && (x.Item3 == "_") && (x.Item4 == dayType)) == null;

                                var departure = ParsedDataLists.DeparturesDictionary[lineNumber].Find(
                                    x => x.Hour == hour &&
                                    x.Minute == minute &&
                                    x.StopGroupNumber == groupNumber &&
                                    x.PostNumber == postNumber &&
                                    x.DayTypeSymbol == dayType &&
                                    x.CourseId == courseId
                                    );

                                if (departure == null)
                                {
                                    var dep = new Entity.Entities.Departure()
                                    {
                                        BrigadeNumber = null,
                                        CourseId = courseId,
                                        Hour = hour,
                                        Minute = minute,
                                        DayTypeSymbol = dayType,
                                        LineNumber = lineNumber,
                                        StopGroupNumber = groupNumber,
                                        PostNumber = postNumber,
                                        IsLowFloor = isLowFloor,
                                        IsArrival = false,
                                        IsPublished = true
                                    };
                                    ParsedDataLists.DeparturesDictionary[lineNumber].Add(dep);
                                }

                            }

                            string closeDeparturesSubsection = reader.ReadLine();
                            if (closeDeparturesSubsection.Replace(" ", string.Empty) != "#OD")
                                throw new Exception("Error closing section OD");
                        }

                        string closeDayTypesSection = reader.ReadLine();
                        if (closeDayTypesSection.Replace(" ", string.Empty) != "#TD")
                            throw new Exception("Error closing section TD");

                        //opis rozkładu z przystanku - sekcja OP
                        int descriptionLinesCount = int.Parse(reader.ReadLine().Substring(21).Replace(" ", string.Empty));

                        DateTime validFrom = new DateTime(1990, 1, 1);
                        string commonDescription = string.Empty;
                        Dictionary<string, string> courseDescription = new Dictionary<string, string>(); //string, string - oznaczenie, opis

                        string previousSymbol = " ";

                        for (int n = 0; n < descriptionLinesCount; n++)
                        {
                            string descriptionLine = reader.ReadLine();

                            switch (descriptionLine[21])
                            {
                                case 'D':
                                    if (descriptionLine.Contains("/"))
                                        validFrom = DateTime.Parse(descriptionLine.Substring(42, descriptionLine.IndexOf('/') - 42));
                                    else
                                        validFrom = DateTime.Parse(descriptionLine.Substring(42));
                                    break;
                                case 'K':
                                    if (descriptionLine.Length > 25)
                                        commonDescription += (descriptionLine.Substring(25) + Environment.NewLine);
                                    break;
                                case 'S':
                                    if (descriptionLine.Substring(25, 2) == "[]")
                                        continue;

                                    string symbol = descriptionLine.Substring(25, 2).Replace(" ", string.Empty);
                                    if (!string.IsNullOrWhiteSpace(symbol))
                                    {
                                        previousSymbol = symbol;
                                        courseDescription.Add(symbol, descriptionLine.Substring(30));
                                    }
                                    else
                                    {
                                        symbol = previousSymbol;
                                        courseDescription[symbol] += (" " + descriptionLine.Substring(30));
                                    }

                                    break;
                            }
                        }

                        string closeDescriptionSection = reader.ReadLine();
                        if (closeDescriptionSection.Replace(" ", string.Empty) != "#OP")
                            throw new Exception("Error closing section OP");

                        /////ZBIORCZA LEGENDA ODJAZDÓW Z PRZYSTANKU
                        var entry = ParsedDataLists.StopPost_LinesList.FirstOrDefault(x => x.LineNumber == lineNumber &&
                        x.PostNumber == postNumber && x.LineNumber == lineNumber);
                        if (entry != null)
                        {
                            entry.TimetableValidFrom = validFrom;
                            entry.LegendDescription = commonDescription;
                        }

                        //dodanie legendy dla odjazdów
                        List<string> savedToDatabaseSymbols = new List<string>();

                        foreach (Tuple<int, int, string, string> legendEntry in departureLegend)
                        {

                            var departure = ParsedDataLists.DeparturesDictionary[lineNumber].FirstOrDefault(x =>
                                            x.LineNumber == lineNumber &&
                                            x.StopGroupNumber == groupNumber &&
                                            x.PostNumber == postNumber &&
                                            x.Hour == legendEntry.Item1 &&
                                            x.Minute == legendEntry.Item2 &&
                                            x.DayTypeSymbol == legendEntry.Item4
                                            );

                            if (legendEntry.Item3 == "_")
                                continue;

                            if (departure != null)
                            {
                                string[] departureLegendArray = departure.LegendSymbols.Split(';');
                                if (departureLegendArray != null && departureLegendArray.Length > 0 && !departureLegendArray.Contains(legendEntry.Item3))
                                        departure.LegendSymbols += (";" + legendEntry.Item3);

                                departure.LegendSymbols = departure.LegendSymbols.Trim(';');
                            }

                            if (!savedToDatabaseSymbols.Contains(legendEntry.Item3))
                            {
                                ParsedDataLists.TimetableLegendsList.Add(new Entity.Entities.TimetableLegend()
                                {
                                    LineNumber = lineNumber,
                                    StopGroupNumber = groupNumber,
                                    StopPostNumber = postNumber,

                                    Symbol = legendEntry.Item3,
                                    Description = courseDescription[legendEntry.Item3]
                                });
                                savedToDatabaseSymbols.Add(legendEntry.Item3);
                            }
                        }
                    }
                    string closeStopsSection = reader.ReadLine();
                    if (closeStopsSection.Replace(" ", string.Empty) != "#RP")
                        throw new Exception("Error closing section RP");

                }
                string closeSubSection = reader.ReadLine();
                if (closeSubSection.Replace(" ", string.Empty) != "#TR")
                    throw new Exception("Error closing section TR");

                //WK - przebiegi kursów
                string coursesSectionOpening = reader.ReadLine();
                int departuresCount = int.Parse(coursesSectionOpening.Substring(9).Replace(" ", string.Empty));

                for (int j = 0; j < departuresCount; j++)
                {
                    //Console.WriteLine($"Odjazd ze spisu  {j} // {departuresCount}");

                    string departureLine = reader.ReadLine();

                    string postNumber = departureLine.Substring(32, 2);
                    string groupNumber = departureLine.Substring(28, 4);
                    string courseId = departureLine.Substring(9, 17);
                    string dayType = departureLine.Substring(35, 2);

                    //W ROZKŁADZIE JAZDY JEST GODZINA 25 JAKO 1-SZA W NOCY
                    int hour = int.Parse(departureLine.Substring(38, 2).Replace(" ", string.Empty)) % 24;
                    int minute = int.Parse(departureLine.Substring(41, 2).Replace(" ", string.Empty));

                    var departure = ParsedDataLists.DeparturesDictionary[lineNumber].Find(
                        x => x.Hour == hour && 
                        x.Minute == minute &&
                        x.StopGroupNumber == groupNumber && 
                        x.PostNumber == postNumber &&
                        x.DayTypeSymbol == dayType &&
                        x.CourseId == courseId
                        );

                    bool toAdd = false;
                    if (departure == null)
                    {
                        toAdd = true;
                        departure = new Entity.Entities.Departure()
                        {
                            CourseId = courseId,
                            Hour = hour,
                            Minute = minute,
                            IsArrival = false,
                            IsLowFloor = true,
                            IsPublished = false,
                            LineNumber = lineNumber,
                            PostNumber = postNumber,
                            DayTypeSymbol = dayType,
                            StopGroupNumber = groupNumber
                        };
                    }
                    if (departureLine[45] == 'B')
                    {
                        departure.IsPublished = false;
                    }
                    else if (departureLine[45] == 'P')
                    {
                        departure.IsArrival = true;
                    }

                    if(toAdd)
                        ParsedDataLists.DeparturesDictionary[lineNumber].Add(departure);
                }

                string closeCoursesSection = reader.ReadLine();
                if (closeCoursesSection.Replace(" ", string.Empty) != "#WK")
                    throw new Exception("Error closing section WK");
            }

            string closeSection = reader.ReadLine();
            if (closeSection.Replace(" ", string.Empty) != "#LL")
                throw new Exception("Error closing section LL");
        }
    }
}
