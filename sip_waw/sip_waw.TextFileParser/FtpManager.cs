﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.TextFileParser
{
    public static class FtpManager
    {
        public static List<string> GetFilesList(string address)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(address);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;

            List<string> returnValue = new List<string>();
            string[] list = null;

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }

            foreach (string line in list)
            {
                // Windows FTP Server Response Format
                // DateCreated    IsDirectory    Name
                string data = line;

                // Parse name
                string name = data.Substring(55);

                returnValue.Add(name);
            }

            return returnValue;
        }

        public static void Download(string server, string remoteFile, string localFile)
        {
            // Get the object used to communicate with the server.  
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(server + "/" + remoteFile);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();

            var fs = File.Create(localFile);
            responseStream.CopyTo(fs);
            fs.Close();

            responseStream.Close();
            response.Close();
        }
    }
}
