﻿using sip_waw.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.TextFileParser
{
    public class QuickDatabaseSaver
    {
        private readonly ApplicationDbContext applicationDbContext;
        private readonly ParsedData parsedData;

        public void UpdateStopType()
        {
            foreach (var post in parsedData.StopPostsList)
            {
                var stopPostLines = parsedData.StopPost_LinesList.Where(x => x.PostGroupNumber == post.StopGroupNumber && x.PostNumber == post.PostNumber).ToList();

                IEnumerable<string> lineTypesOnStop = stopPostLines.Join(parsedData.LinesList, x => x.LineNumber, y => y.Number, (x, y) => y.Description).Distinct().ToList();

                if (lineTypesOnStop.FirstOrDefault(x => x.Contains("KOLEI")) != null)
                    post.StopType = Entity.Enums.StopType.Kolejowy;
                else if (lineTypesOnStop.Contains("LINIA TRAMWAJOWA"))
                {
                    if (lineTypesOnStop.FirstOrDefault(x => x.Contains("ZWYKŁA") || x.Contains("STREFOWA") || x.Contains("NOCNA") || x.Contains("PRZYSPIESZONA") || x.Contains("EKSPRESOWA")) != null)
                    {
                        post.StopType = Entity.Enums.StopType.TramwajowoAutobusowy;
                    }
                    else
                    {
                        post.StopType = Entity.Enums.StopType.Tramwajowy;
                    }
                }
            }

            applicationDbContext.SaveChanges();
        }

        public QuickDatabaseSaver(ApplicationDbContext dbContext, ParsedData pData)
        {
            applicationDbContext = dbContext;
            parsedData = pData;
        }

        public void SaveToDatabase()
        {
            applicationDbContext.Database.CommandTimeout = 100000;

            Console.WriteLine("Zapis typów dni");
            SaveDayTypes();

            Console.WriteLine("Zapis linii");
            SaveLines();

            Console.WriteLine("Zapis kalendarza kursowania");
            SaveTimetableCalendar();

            Console.WriteLine("Zapis miast i wsi");
            SaveTowns();

            Console.WriteLine("Zapis zespołów przystankowych");
            SaveStopGroups();

            Console.WriteLine("Zapis słupków przystankowych");
            SaveStopPosts();

            Console.WriteLine("Zapis linii na słupkach");
            SaveStopPost_Lines();

            Console.WriteLine("Zapis typów tras");
            SaveRouteTypes();

            Console.WriteLine("Zapis przystanków na trasach");
            SaveStopPost_RouteTypes();

            Console.WriteLine("Zapis odjazdów wszystkich linii");
            SaveDepartures();

            Console.WriteLine("Zapis legendy odjazdów");
            SaveTimetableLegend();
        }

        private void SaveDayTypes()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[DayTypes]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var dayType in parsedData.DayTypesList)
            {
                var row = table.NewRow();

                row["Symbol"] = dayType.Symbol;
                row["Description"] = dayType.Description;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "DayTypes";
                bulk.WriteToServer(table);
            }
        }


        private void SaveDepartures()
        {
            foreach (var dictEntry in parsedData.DeparturesDictionary)
            {
                SaveDepartures(dictEntry.Value);
            }
        }

        private void SaveDepartures(IEnumerable<Entity.Entities.Departure> departures)
        {
            var table = new System.Data.DataTable();

            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[Departures]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            };

            foreach (var dep in departures)
            {
                var row = table.NewRow();

                row["Id"] = dep.Id;
                row["CourseId"] = dep.CourseId;
                row["LineNumber"] = dep.LineNumber;
                row["StopGroupNumber"] = dep.StopGroupNumber;
                row["PostNumber"] = dep.PostNumber;
                row["BrigadeNumber"] = dep.BrigadeNumber;
                row["Hour"] = dep.Hour;
                row["Minute"] = dep.Minute;
                row["DayTypeSymbol"] = dep.DayTypeSymbol;
                row["IsPublished"] = dep.IsPublished;
                row["IsArrival"] = dep.IsArrival;
                row["IsLowFloor"] = dep.IsLowFloor;
                row["LegendSymbols"] = dep.LegendSymbols;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "Departures";
                bulk.WriteToServer(table);
            }
        }

        private void SaveLines()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[Lines]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var line in parsedData.LinesList)
            {
                var row = table.NewRow();

                row["Number"] = line.Number;
                row["Description"] = line.Description;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "Lines";
                bulk.WriteToServer(table);
            }
        }

        private void SaveRouteTypes()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[RouteTypes]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var routeType in parsedData.RouteTypesList)
            {
                var row = table.NewRow();

                row["Name"] = routeType.Name;
                row["LineNumber"] = routeType.LineNumber;
                row["RouteDirection"] = routeType.RouteDirection;
                row["RouteLevel"] = routeType.RouteLevel;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "RouteTypes";
                bulk.WriteToServer(table);
            }
        }

        private void SaveStopGroups()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[StopGroups]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var stopGroup in parsedData.StopGroupsList)
            {
                var row = table.NewRow();

                row["Number"] = stopGroup.Number;
                row["Name"] = stopGroup.Name;
                row["TownCode"] = stopGroup.TownCode;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "StopGroups";
                bulk.WriteToServer(table);
            }
        }

        private void SaveStopPosts()
        {
            //var table = new System.Data.DataTable();
            //using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[StopPosts]", applicationDbContext.Database.Connection.ConnectionString + ";Type System Version=SQL Server 2012"))
            //{
            //    adapter.Fill(table);
            //}

            //foreach (var stopPost in parsedData.StopPostsList)
            //{
            //    var row = table.NewRow();

            //    row["StopGroupNumber"] = stopPost.StopGroupNumber;
            //    row["PostNumber"] = stopPost.PostNumber;
            //    row["StreetName"] = stopPost.StreetName;
            //    row["CommonDestination"] = stopPost.CommonDestination;
            //    row["StopType"] = stopPost.StopType;
            //    row["Position"] = stopPost.Position;

            //    table.Rows.Add(row);
            //}

            //using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            //{
            //    bulk.DestinationTableName = "StopPosts";
            //    bulk.WriteToServer(table);
            //}

            applicationDbContext.StopPosts.AddRange(parsedData.StopPostsList);
            applicationDbContext.SaveChanges();
        }

        private void SaveStopPost_Lines()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[StopPost_Line]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var stopPost_Line in parsedData.StopPost_LinesList)
            {
                var row = table.NewRow();

                row["LineNumber"] = stopPost_Line.LineNumber;
                row["PostGroupNumber"] = stopPost_Line.PostGroupNumber;
                row["PostNumber"] = stopPost_Line.PostNumber;
                row["StoppingCharacter"] = stopPost_Line.StoppingCharacter;
                row["IsPublished"] = stopPost_Line.IsPublished;

                row["LegendDescription"] = stopPost_Line.LegendDescription;

                if (stopPost_Line.TimetableValidFrom < new DateTime(1800, 1, 1) || stopPost_Line.TimetableValidFrom > new DateTime(9000, 1, 1))
                    row["TimetableValidFrom"] = new DateTime(1990, 1, 1);
                else
                    row["TimetableValidFrom"] = stopPost_Line.TimetableValidFrom;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "StopPost_Line";
                bulk.WriteToServer(table);
            }
        }

        private void SaveStopPost_RouteTypes()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[StopPost_RouteType]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var stopPost_RouteType in parsedData.StopPost_RouteTypesList)
            {
                var row = table.NewRow();

                row["PostGroupNumber"] = stopPost_RouteType.PostGroupNumber;
                row["PostNumber"] = stopPost_RouteType.PostNumber;
                row["RouteTypeName"] = stopPost_RouteType.RouteTypeName;
                row["LineNumber"] = stopPost_RouteType.LineNumber;
                row["StopNumber"] = stopPost_RouteType.StopNumber;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "StopPost_RouteType";
                bulk.WriteToServer(table);
            }
        }

        private void SaveTimetableCalendar()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[TimetableCalendars]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var timetableCalendarDate in parsedData.TimetableCalendarsList)
            {
                var row = table.NewRow();

                row["Date"] = timetableCalendarDate.Date;
                row["LineNumber"] = timetableCalendarDate.LineNumber;
                row["DayTypeSymbol"] = timetableCalendarDate.DayTypeSymbol;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "TimetableCalendars";
                bulk.WriteToServer(table);
            }
        }

        private void SaveTimetableLegend()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[TimetableLegends]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var timetableLegendEntry in parsedData.TimetableLegendsList)
            {
                var row = table.NewRow();

                row["Id"] = timetableLegendEntry.Id;
                row["StopGroupNumber"] = timetableLegendEntry.StopGroupNumber;
                row["StopPostNumber"] = timetableLegendEntry.StopPostNumber;
                row["LineNumber"] = timetableLegendEntry.LineNumber;
                row["Symbol"] = timetableLegendEntry.Symbol;
                row["Description"] = timetableLegendEntry.Description;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "TimetableLegends";
                bulk.WriteToServer(table);
            }
        }

        private void SaveTowns()
        {
            var table = new System.Data.DataTable();
            using (var adapter = new System.Data.SqlClient.SqlDataAdapter($"SELECT TOP 0 * FROM [rozklad].[dbo].[Towns]", applicationDbContext.Database.Connection.ConnectionString))
            {
                adapter.Fill(table);
            }

            foreach (var town in parsedData.TownsList)
            {
                var row = table.NewRow();

                row["Code"] = town.Code;
                row["Name"] = town.Name;

                table.Rows.Add(row);
            }

            using (var bulk = new System.Data.SqlClient.SqlBulkCopy(applicationDbContext.Database.Connection.ConnectionString))
            {
                bulk.DestinationTableName = "Towns";
                bulk.WriteToServer(table);
            }
        }

        public void EraseDatabase()
        {
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
            {
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("if exists(select * from sys.databases where name = 'rozklad') EXEC sys.sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'", connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                command.Dispose();

                command = new System.Data.SqlClient.SqlCommand("if exists(select * from sys.databases where name = 'rozklad') EXEC sys.sp_msforeachtable 'DELETE FROM ?'", connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                command.Dispose();

                command = new System.Data.SqlClient.SqlCommand("if exists(select * from sys.databases where name = 'rozklad') EXEC sys.sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'", connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                command.Dispose();
            }
        }
    }
}
