﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using sip_waw.Entity.Entities;

namespace sip_waw.TextFileParser
{
    public class ParsedData
    {
        public List<BrigadeLine> BrigadeLinesList { get; set; }

        public List<DayType> DayTypesList { get; set; }

        public Dictionary<string, List<Departure>> DeparturesDictionary { get; set; }
        // public List<Departure> DeparturesList { get; set; }

        public List<Line> LinesList { get; set; }

        public List<RouteType> RouteTypesList { get; set; }

        public List<StopGroup> StopGroupsList { get; set; }

        public List<StopPost_Line> StopPost_LinesList { get; set; }

        public List<StopPost_RouteType> StopPost_RouteTypesList { get; set; }

        public List<StopPost> StopPostsList { get; set; }

        public List<TimetableCalendar> TimetableCalendarsList { get; set; }

        public List<TimetableLegend> TimetableLegendsList { get; set; }

        public List<Town> TownsList { get; set; }

        public ParsedData()
        {
            BrigadeLinesList = new List<BrigadeLine>();

            DayTypesList = new List<DayType>();

            DeparturesDictionary = new Dictionary<string, List<Departure>>();
            //DeparturesList = new List<Departure>();

            LinesList = new List<Line>();

            RouteTypesList = new List<RouteType>();

            StopGroupsList = new List<StopGroup>();

            StopPost_LinesList = new List<StopPost_Line>();

            StopPost_RouteTypesList = new List<StopPost_RouteType>();

            StopPostsList = new List<StopPost>();

            TimetableCalendarsList = new List<TimetableCalendar>();

            TimetableLegendsList = new List<TimetableLegend>();

            TownsList = new List<Town>();
        }
    }
}
