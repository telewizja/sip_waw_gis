﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sip_waw.Entity;
using sip_waw.TextFileParser;

namespace sip_waw.TextFileParserConsole
{
    class Program
    {
        private const string ServerPath = "ftp://rozklady.ztm.waw.pl/";

        private static void SaveDataToDatabase(ApplicationDbContext dbContext, ParsedData pData)
        {
            dbContext.DayTypes.AddRange(pData.DayTypesList);
            dbContext.Lines.AddRange(pData.LinesList);
            dbContext.TimetableCalendars.AddRange(pData.TimetableCalendarsList);

            //
            dbContext.SaveChanges();
            //

            if(pData.TownsList.FirstOrDefault(x => x.Code == "--") == null)
                pData.TownsList.Add(new Entity.Entities.Town() { Code = "--", Name = "WARSZAWA" });
            dbContext.Towns.AddRange(pData.TownsList);

            dbContext.StopGroups.AddRange(pData.StopGroupsList);
            dbContext.StopPosts.AddRange(pData.StopPostsList);

            //
            dbContext.SaveChanges();
            //

            dbContext.StopPost_Lines.AddRange(pData.StopPost_LinesList);
            dbContext.RouteTypes.AddRange(pData.RouteTypesList);

            dbContext.StopPost_RouteTypes.AddRange(pData.StopPost_RouteTypesList);

            //
            dbContext.SaveChanges();
            //

            //dbContext.Departures.AddRange(pData.DeparturesList);
            foreach(var dictEntry in pData.DeparturesDictionary)
            {
                dbContext.Departures.AddRange(dictEntry.Value);

                //
                dbContext.SaveChanges();
                //
            }

            dbContext.TimetableLegends.AddRange(pData.TimetableLegendsList);

            //
            dbContext.SaveChanges();
            //
        }

        static void Main(string[] args)
        {
            string argsFilePath = Path.GetTempPath();

            if(args.Length < 1)
            {
                Console.WriteLine("File unspecified! Writing to temp folder: " + Path.GetTempPath());
                //return;
            }
            else
            {
                argsFilePath = args[0];
            }

            if (!Directory.Exists(argsFilePath))
            {
                Directory.CreateDirectory(argsFilePath);
            }

            Console.WriteLine("Pobieranie listy plików z ztm");
            List<string> files = FtpManager.GetFilesList(ServerPath);
            files.Reverse();

            string currentFileName = files[0];

            foreach (var fileName in files)
            {
                int day = int.Parse(fileName.Substring(6, 2));
                int month = int.Parse(fileName.Substring(4, 2));
                int year = int.Parse(fileName.Substring(2, 2)) + 2000;
                DateTime fileValidFromDate = new DateTime(year, month, day);

                if (fileValidFromDate < DateTime.Now)
                {
                    currentFileName = fileName;
                    break;
                }
            }

            Console.WriteLine("Pobieranie obowiązującego pliku z rozkładem");
            FtpManager.Download(ServerPath, currentFileName, Path.Combine(argsFilePath, currentFileName));

            Console.WriteLine("Rozpakowanie pliku");
            var processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = @"C:\Program Files\7-Zip\7z.exe";
            processStartInfo.Arguments = "e " + Path.Combine(argsFilePath, currentFileName) + " -y -o" + argsFilePath;

            var pr = Process.Start(processStartInfo);
            pr.WaitForExit();

            SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
            SqlProviderServices.SqlServerTypesAssemblyName =
    "Microsoft.SqlServer.Types, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91";

          
            TextFileParser.TextFileParser parser = new TextFileParser.TextFileParser();

            Console.WriteLine("Łączenie lub tworzenie bazy danych");
            parser.AppDbContext = new ApplicationDbContext();
            parser.AppDbContext.Database.CreateIfNotExists();

            Console.WriteLine("Parsowanie pliku");
            parser.Filepath = Path.Combine(argsFilePath, currentFileName.Replace("7z", "txt"));
            parser.ParseText();

            //SaveDataToDatabase(parser.AppDbContext, parser.ParsedDataLists);

            QuickDatabaseSaver qsaver = new QuickDatabaseSaver(parser.AppDbContext, parser.ParsedDataLists);

            Console.WriteLine("Aktualizacja typów przystanków");
            qsaver.UpdateStopType();

            Console.WriteLine("Czyszczenie bazy danych...");
            qsaver.EraseDatabase();

            Console.WriteLine("Zapis do bazy danych");
            qsaver.SaveToDatabase();

            Console.WriteLine("Kasowanie plików");
            File.Delete(Path.Combine(argsFilePath, currentFileName));
            File.Delete(Path.Combine(argsFilePath, currentFileName.Replace("txt", "7z")));
            Directory.Delete(argsFilePath, true);

        }
    }
}
