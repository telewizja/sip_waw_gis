﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using sip_waw.Entity;
using sip_waw.Entity.Entities;

namespace sip_waw.Models
{
    public static class ApiWarszawa
    {
        private const string apiKey = "e91cf175-bc88-4cd9-85b2-66af7636829f";

        private const string resourceId = "f2e5503e-927d-4ad3-9500-4ab9e55deb59";

        private static string url = $"https://api.um.warszawa.pl/api/action/busestrams_get/?resource_id={resourceId}&apikey={apiKey}";

        private static bool GetLineType(string lineNumber, out int type)
        {
            type = 0;
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var l = dbContext.Lines.Find(lineNumber);
                if (l == null)
                    return false;

                if (l.Description.ToUpper().Contains("TRAMWA"))
                    type = 2;
                else if (l.Description.ToUpper().Contains("KOLE"))
                    type = -1;
                else
                    type = 1;
            }

            return true;
        }

        public static VehicleLiveData[] GetVehiclesForLine(string line)
        {
            if (GetLineType(line, out int tramBus))
            {
                string parametrizedUrl = url + $"&type={tramBus}" + $"&line={line}";

                WebRequest request = WebRequest.Create(parametrizedUrl);

                string data = string.Empty;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    data = reader.ReadToEnd();
                }

                try
                {
                    var t = JsonConvert.DeserializeObject<Dictionary<string, VehicleLiveData[]>>(data);
                    return t["result"];
                }
                catch (Exception e)
                {
                    return null;
                }


            }

            return null;
        }

        public static VehicleLiveData[] GetAllVehicles(int tramBus)
        {
            string parametrizedUrl = url + $"&type={tramBus}";

            WebRequest request = WebRequest.Create(parametrizedUrl);

            string data = string.Empty;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                data = reader.ReadToEnd();
            }

            try
            {
                var t = JsonConvert.DeserializeObject<Dictionary<string, VehicleLiveData[]>>(data);
                return t["result"];
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public static VehicleLiveData[] GetAllVehicles()
        {
            List<VehicleLiveData> t1 = GetAllVehicles(1).ToList();
            t1.AddRange(GetAllVehicles(2).ToList());

            return t1.ToArray();

        }

        public static VehicleLiveData[] GetVehiclesForStop(string group, string post)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                StopPost stop = dbContext.StopPosts.Find(group, post);
                if (stop != null)
                {
                    var lines = stop.StopPost_Lines.Select(x => x.LineNumber).Distinct();

                    List<VehicleLiveData> ret = new List<VehicleLiveData>();
                    foreach (string lineNum in lines)
                    {
                        ret.AddRange(GetVehiclesForLine(lineNum).ToList());
                    }

                    return ret.ToArray();
                }
                else
                {
                    return new VehicleLiveData[0];
                }
            }
        }
    }
}