﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace sip_waw.Models
{
    public class VehicleLiveData
    {
        public string Brigade { get; set; }
        public string Lines { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Time { get; set; }
    }
}