﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Informacje ogólne o zestawie zależą od poniższego 
// zestawu atrybutów. Zmień wartości tych atrybutów, aby zmodyfikować informacje
// związane z zestawem.
[assembly: AssemblyTitle("sip_waw")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("sip_waw")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Ustawienie dla atrybutu ComVisible wartości false powoduje, że typy w tym zestawie stają się niewidoczne 
// dla składników COM. Jeśli musisz uzyskiwać dostęp do typu w tym zestawie 
// z modelu COM, ustaw dla atrybutu ComVisible tego typu wartość true.
[assembly: ComVisible(false)]

// Poniższy identyfikator GUID odpowiada atrybutowi ID biblioteki typów, jeśli ten projekt jest uwidaczniany w modelu COM
[assembly: Guid("50064173-7ef6-4c65-a5c0-94c580a341d6")]

// Informacje o wersji zestawu obejmują następujące cztery wartości:
//
//      Wersja główna
//      Wersja pomocnicza 
//      Numer kompilacji
//      Poprawka
//
// Możesz określić wszystkie te wartości lub użyć wartości domyślnych numerów poprawki i kompilacji, 
// stosując znak „*”, jak pokazano poniżej:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
