var prevprevPanel = null;
var prevPanel = null;
var actualPanel = "Line-panel";
$(document).ready(function () {

    window.dispatchEvent(new Event('resize'));
    $(".allContent").height($("body").height() - $("#nav-area").height());

    $('#byLine').on('click', function () {

        stopDisplayingVehicles();

        if ($("#controlPanel").width() == 0) { //to całkiem zabawna sprawa.....
            openControlPanel();
        }
        $("#Posts-panel").css("display", "none")
        $("#Posts-panel-detail").css("display", "none");
        $("#Line-panel-routes").css("display", "none")
        $("#Lines-content").empty();
        $("#line-text-input").val("");
        doneTypingLine();
        $("#Line-panel").css("display", "block");
        actualPanel = "Line-panel";
    });

    $('#byPost').on('click', function () {

        stopDisplayingVehicles();

        if ($("#controlPanel").width() == 0) {
            openControlPanel();
        }
        $("#Line-panel-routes").css("display", "none")
        $("#Line-panel").css("display", "none");
        $("#Posts-panel-detail").css("display", "none");
        $("#Posts-content").empty();
        $("#post-text-input").val("");
        $("#Posts-panel").css("display", "block")
        actualPanel = "Posts-panel";
    });

    $('#AllVehicles').on('click', function () {

        stopDisplayingVehicles();
        if ($("#controlPanel").width() > 0) {
            closeControlPanel();
        }

        var vehicles = [];

        allVehicles = true;
        getAllVehicles();
    });


});


window.onresize = function (event) {
    $(".allContent").height($("body").height() - $("#nav-area").height());
};



//var panelOpened = false;

//function switchControlPanel() {
//    if (panelOpened) {
//        closeControlPanel();
//    }
//    else {
//        openControlPanel();
//    }
//}

function closeControlPanel() {
    $("#controlPanel").animate({ width: 0 }, 500, function () {
        $("#btn-back img").css("display", "none");
        $("#btn-back-post img").css("display", "none");
        $("#line-text-input").css("display", "none");
        $("#post-text-input").css("display", "none");
        $("#btn-expand").css("display", "initial");
        panelOpened = false;
    })


}

function openControlPanel() {
    $("#btn-back img").css("display", "initial");
    $("#btn-back-post img ").css("display", "initial");
    $("#line-text-input").css("display", "block");
    $("#post-text-input").css("display", "block");
    $("#btn-expand").css("display", "none");

    $("#controlPanel").animate({ width: "20%" }, 500, function () {

        panelOpened = true;
    })
}

function getBackFromLineRoutes() {
    stopDisplayingVehicles();

    $("#Line-panel-routes").css("display", "none");
    $(`#${prevPanel}`).css("display", "block");
    actualPanel = prevPanel;
    prevPanel = prevprevPanel;

}

function getBackFromPostDetails() {
    $("#Posts-panel-detail").css("display", "none");
    $(`#${prevPanel}`).css("display", "block");
    actualPanel = prevPanel;
    prevPanel = prevprevPanel;

    stopDisplayingVehicles();
}
var lineNum = null;
function ShowLinesPanel(btn, param) {
    stopDisplayingVehicles();
    var line = null;
    $("#Line-panel").css("display", "none");
    $("#time-table-content").css("display", "none");
    $("#time-table-content ul").empty();
    $("#TimeTableHeading").css("display", "none");
    if ($("#controlPanel").width() == 0) {
        openControlPanel();
    }
    if (param == null) {
        prevprevPanel = prevPanel;
        prevPanel = "Line-panel";
        actualPanel = "Line-panel-routes";
        line = btn.text();
    }
    else {
        if (actualPanel != "Line-panel-routes") {
            $(`#${actualPanel}`).css("display", "none");
            prevprevPanel = prevPanel;
            prevPanel = actualPanel;
            actualPanel = "Line-panel-routes";
        }
        line = param.line;
    }

    var RoutesPanel = $("#Line-panel-routes");
    RoutesPanel.children("#RoutesHeading").text(`Kierunki dla linii ${line}`);

    vehiclesLine = line;
    getVehiclesForLine();

    RoutesPanel.css("display", "block");
    lineNum = line;
    $.ajax({
        url: `Home/GetRouteTypes?lineNumber=${line}`,
        type: "GET",
        success: function (result) {
            var json = result
            handleResponeRoutes(json);

        }
    });

    function handleResponeRoutes(data) {
        var container = $("#Routes-content");
        container.empty();

        for (i = 0; i < data.length; i++) {
            var toAdd = $('<div/>', {
                class: "line-route",
            });
            toAdd.data("details", data[i]);
            var Path = $('<h4/>', {
                text: `${data[i].FirstStop} >> ${data[i].LastStop}`
            });
            toAdd.append(Path);

            container.append(toAdd);
        }

    }

}


function LinePanel() {
    doneTypingLine();
    var typingTimer;
    var doneTypingInterval = 500;

    //on keyup, start the countdown
    $("#line-text-input").keyup(function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTypingLine, doneTypingInterval);
    });





    $('body').on('click', '.line-box', function () { ShowLinesPanel($(this), null); });


    $('body').on('click',
        '.line-route',
        function () {

            stopDisplayingVehicles();

            $("#time-table-content").css("display", "block");
            $("#Routes-content .route-clicked").removeClass("route-clicked");
            var btn = $(this);
            btn.addClass("route-clicked");
            $.ajax({
                url: `Home/GetRouteStops?lineNumber=${lineNum}&route=${btn.data("details").Name}`,
                type: "GET",
                success: function (result) {
                    var json = result
                    handleResponeTimeTable(json);
                }
            });


        });
    function handleResponeTimeTable(data) {
        var container = $("#time-table-content ul");
        container.empty();
        $("#TimeTableHeading").css("display", "block");
        var points = [];
        var stops = [];

        for (i = 0; i < data.length; i++) {
            var toAdd = $('<li/>', {
                text: data[i].Name,
                class: "list-group-item"
            });
            toAdd.data("details", data[i]);
            points.push(
                {
                    longitude: data[i].Longitude,
                    latitude: data[i].Latitude
                }
            );

            stops.push(
                {
                    Longitude: data[i].Longitude,
                    Latitude: data[i].Latitude,
                    Group: data[i].GroupNumber,
                    Post: data[i].PostNumber,
                    Name: data[i].Name,
                    Type: data[i].Type
                }
            );

            container.append(toAdd);
        }

        displayRouteOnMap(points, stops);
    }
    $('#time-table-content').on('click', 'li',
        function () {
            var li = $(this);
            var detailData = li.data("details");
            displaySingleStop([{
                name: detailData.Name,
                group: detailData.GroupNumber,
                post: detailData.PostNumber,
                type: detailData.Type,
                longitude: detailData.Longitude,
                latitude: detailData.Latitude
            }]);
            displayStopDetails(true, { group: detailData.GroupNumber, post: detailData.PostNumber });
        });
}
function doneTypingLine() {
    var input = $("#line-text-input");
    $.ajax({
        url: `Home/GetLines?searchQuery=${input.val()}`,
        type: "GET",
        success: function (result) {
            var json = result
            handleResponeGetLines(json);

        }
    });
}

function handleResponeGetLines(data) {
    var container = $("#Lines-content");
    container.empty();
    for (var key in data) {
        var group = $("<div/>", {
            class: "line-type"

        });
        var label = $("<h4/>", {
            text: key
        });

        group.append(label);
        var linesDiv = $("<div/>", {
            class: "lines-in-type"
        });

        for (i = 0; i < data[key].length; i++) {
            var toAdd = $('<span/>', {
                class: "line-box",
                text: data[key][i]
            });
            linesDiv.append(toAdd);
        }
        group.append(linesDiv);
        container.append(group);
    }


};




function PostsPanel() {
    var typingTimer;
    var doneTypingInterval = 500;


    $("#post-text-input").keyup(function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });



    function doneTyping() {
        var input = $("#post-text-input");
        if (input.val().length < 3) {

            return;
        }
        $.ajax({
            url: `Home/GetStopGroups?searchQuery=${input.val()}`,
            type: "GET",
            success: function (result) {
                var json = result
                handleResponeGetStopGroups(json);
            }
        });
    }

    function handleResponeGetStopGroups(data) {
        var container = $("#Posts-content");
        container.empty();

        for (i = 0; i < data.length; i++) {
            var toAdd = $('<div/>', {
                class: "post-group-box",
            });

            var Name = $('<h3/>', {
                text: `${data[i].Name} (${data[i].Town != null ? data[i].Town : "Warszawa"})`
            });
            var Namediv = $('<div/>', {
                class: "post-group-name-div"
            });
            Namediv.append(Name)
            toAdd.append(Namediv);
            var DetailButton = $('<button/>', {
                text: "Szczegóły",
                class: "group-detail-btn",
            });
            DetailButton.data("stopNum", data[i].Number);
            toAdd.append(DetailButton);

            container.append(toAdd);
        }
    }

    $('body').on('click', '.group-detail-btn', function () {

        stopDisplayingVehicles();

        var btn = $(this);
        if (btn.text() == "Szczegóły") {
            $.ajax({
                url: `Home/GetPostsDetails?groupNumber=${btn.data("stopNum")}`,
                type: "GET",
                success: function (result) {
                    var json = result;
                    handleResponePosts(btn, json);

                }
            });
            btn.text("Schowaj");

        }
        else {
            btn.parent(".post-group-box").find(".post-group-stops-div").remove();
            btn.text("Szczegóły");
        }
    });

    function handleResponePosts(btn, data) {
        var parentDiv = btn.parent(".post-group-box");
        var divToAdd = $("<div/>",
            {
                class: "post-group-stops-div"
            });


        for (var key in data) {
            var group = $("<div/>", {
                class: "stop-type"

            });
            var label = $("<h4/>", {
                class: "stop-type-name",
                text: key
            });

            group.append(label);

            for (i = 0; i < data[key].length; i++) {
                var detailDiv = $("<div/>", {
                    class: "stop-detail"
                });
                detailDiv.data("info", data[key][i]);
                var name = $('<h4/>', {
                    text: data[key][i].Name
                });
                var street = $('<h5/>', {
                    text: `ulica: ${data[key][i].Street}`
                });
                var dest =
                    $('<h5/>', {
                        text: `kierunek: ${data[key][i].Dest}`
                    });

                detailDiv.append(name);;
                detailDiv.append(street);
                detailDiv.append(dest);
                group.append(detailDiv);
            }
            divToAdd.append(group);

        }
        parentDiv.find(".post-group-name-div").after(divToAdd);
    }


    $('body').on('click', '.stop-detail', displayStopDetailsClick);

    function displayStopDetailsClick() {
        displayStopDetails(false, $(this));

    }



};

var info = null
function displayStopDetails(externalExec, param) {

    stopDisplayingVehicles();



    if (externalExec) {
        $.ajax({
            url: `Home/GetPost?groupNumber=${param.group}&postNumber=${param.post}`,
            type: "GET",
            async: false,
            success: function (result) {
                info = result;
            }
        });
        vehiclesStop = param.group;
        vehiclesPost = param.post;
    }
    else {
        info = param.data("info");
        displaySingleStop([
            {
                name: info.Name,
                group: info.GroupNumber,
                post: info.PostNumber,
                type: info.Type,
                longitude: info.Longitude,
                latitude: info.Latitude
            }
        ]);
        vehiclesStop = info.GroupNumber;
        vehiclesPost = info.PostNumber;
    }


    getVehiclesForStop();

    ClearPostDetailPanelAndUpdateValues();

    $.ajax({
        url: `Home/GetNextDepartures?groupNumber=${info.GroupNumber}&postNumber=${info.PostNumber}`,
        type: "GET",
        success: function (result) {
            var json = result;
            handleResponeDeparture(json);

        }
    });

    $.ajax({
        url: `Home/GetPostLines?groupNumber=${info.GroupNumber}&postNumber=${info.PostNumber}`,
        type: "GET",
        success: function (result) {
            var lines = result;
            PrepareLinesForPost(lines);
        }
    });

    function handleResponeDeparture(data) {
        var tab = $("#departure-table");
        for (var i = 0; i < data.length; i++) {
            var row = $("<tr/>", {

            });
            if (data[i].IsLowFloor) {
                row.addClass("underline");
            }

            if (data[i].IsPublished == false) {
                row.addClass("grey");
            }
            var line = $("<td/>", {
                text: data[i].LineNumber
            });

            var destination = $("<td/>", {
                text: data[i].Destination
            });

            var hours = data[i].Hour % 24;
            if (hours < 10)
                hours = "0" + hours;
            var minuts = data[i].Minute;
            if (minuts < 10)
                minuts = "0" + minuts;

            var time = $("<td/>", {
                text: `${hours}:${minuts}`
            });
            row.append(line);
            row.append(destination);
            row.append(time);
            tab.append(row);
        }
    }

    function PrepareLinesForPost(lines) {
        var container = $("#Post-lines-content");
        container.empty();
        for (var key in lines) {
            var group = $("<div/>", {
                class: "line-stoptype"

            });
            var label = $("<h4/>", {
                text: key
            });

            group.append(label);

            for (i = 0; i < lines[key].length; i++) {
                var toAdd = $('<span/>', {
                    class: "line-stop-box",
                    text: lines[key][i].LineNumber
                });
                group.append(toAdd);

            }
            container.append(group);
        }

    }

    function ClearPostDetailPanelAndUpdateValues() {
        $(`#${actualPanel}`).css("display", "none");
        if (actualPanel != "Posts-panel-detail") {
            prevprevPanel = prevPanel;
            prevPanel = actualPanel;
            actualPanel = "Posts-panel-detail";
        }

        $("#departurefromPost-table").empty();
        $("#departure-table").empty();
        $("#Post-legend-content").empty()
        $("#PostTimeTableHeading").empty();
        var PostDetailPanel = $("#Posts-panel-detail");
        PostDetailPanel.children("#PostDepartureHeading").text(`Najbliższe odjazdy dla przystanku ${info.Name}`);
        PostDetailPanel.css("display", "block");
        if ($("#controlPanel").width() == 0) {
            openControlPanel();
        }
    }


}

$('body').on('click', '.line-stop-box', function () {
    var btn = $(this);
    $("#PostTimeTableHeading").text(`Odjazdy dla linii ${btn.text()}`);
    var line = btn.text();
    $.ajax({
        url: `Home/GetTimetableTable?groupNumber=${info.GroupNumber}&postNumber=${info.PostNumber}&lineNumber=${line}`,
        type: "GET",
        success: function (result) {
            var json = result;
            handleResponeLinePostTimeTable(json);

        }
    });

});
function handleResponeLinePostTimeTable(data) {
    var container = $("#departurefromPost-table");
    container.empty();

    var container2 = $("#Post-legend-content");
    container2.empty();
    for (var key in data["deps"]) {
        var row = $("<tr/>", {

        });
        //if (data["deps"][i].isLowFloor) {
        //    row.addClass("underline");
        //}

        //if (data[i].isPublished == false) {
        //    row.addClass("grey");
        //}

        var hours = key % 24;
        if (hours < 10)
            hours = "0" + hours;
        var hourTD = $("<td/>", {
            text: `${hours}`,
            class: "shrinktd"
        });
        row.append(hourTD);
        var minuteText = "";
        for (var i = 0; i < data["deps"][key].length; i++) {
            var minuts = data["deps"][key][i].Minute;
            if (minuts < 10)
                minuts = "0" + minuts;
            var legend = data["deps"][key][i].Legend;
            minuteText += ` ${minuts}${legend != null ? legend : ""}`
        }

        var time = $("<td/>", {
            text: minuteText
        });

        row.append(time);
        container.append(row);
    }
    for (var i = 0; i < data["legends"].length; i++) {
        var leg = $("<p/>", {
            text: `${data["legends"][i].Symbol} - ${data["legends"][i].Description}`
        });
        container2.append(leg);
    }

}

function stopDisplayingVehicles() {
    vehiclesLine = null;
    allVehicles = null;
    vehiclesStop = null;

    clearRoutesOnMap();
    vehiclesSource.clear();
}

var vehiclesLine = null;
function getVehiclesForLine() {
    if (vehiclesLine != null) {
        clearRoutesOnMap();

        $.ajax({
            url: "Home/GetVehiclesForLine",
            type: "GET",
            data: {
                line: vehiclesLine
            },
            success: function (result) {
                displayVehiclesOnMap(result);
                setTimeout(getVehiclesForLine, 10 * 1000);
            }
        });
    }
}

var allVehicles = null;
function getAllVehicles() {
    if (allVehicles != null) {
        clearRoutesOnMap();

        $.ajax({
            url: "Home/GetAllVehicles",
            type: "GET",
            success: function (result) {
                displayVehiclesOnMap(result);
                setTimeout(getAllVehicles, 10 * 1000);
            }
        });
    }
}

var vehiclesStop = null;
var vehiclesPost;
function getVehiclesForStop() {
    if (vehiclesStop != null) {
        clearRoutesOnMap();

        $.ajax({
            url: "Home/GetVehiclesForStop",
            type: "GET",
            data: {
                group: vehiclesStop,
                post: vehiclesPost
            },
            success: function (result) {
                displayVehiclesOnMap(result);
                setTimeout(getAllVehicles, 10 * 1000);
            }
        });
    }
}

////TO DO
function openSingleStopPanel(group, post) {
    console.log("open stop panel" + group + " " + post);
    displayStopDetails(true, { group, post });
}

function openLinePanel(line) {
    ShowLinesPanel(null, { line });
    console.log("open line panel : " + line);

}


LinePanel();
PostsPanel();