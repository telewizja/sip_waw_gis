//function success(pos) {
//	var crd = pos.coords;

//    console.log('Your current position is:');
//    console.log('Latitude : ${crd.latitude}');
//    console.log('Longitude: ${crd.longitude}');
//    console.log('More or less ${crd.accuracy} meters.');

//    (crd.longitude, crd.latitude);

//	positionLayer.addFeature(new ol.Feature(new ol.geom.Point(crd.latitude, crd.longitude)));
//}

//function error(err) {
//	console.warn('ERROR(${err.code}): ${err.message}');
//}

//function addPositionToMap(){
//	var positionSource = new ol.source.Vector();

//	var positionLayer = new ol.layer.Vector({
//		source: positionSource
//	});

//	var options = {
//		enableHighAccuracy: true,
//		timeout: 5000,
//		maximumAge: 0
//	};

//	navigator.geolocation.getCurrentPosition(success, error, options);

//	positionLayer.setStyle(new ol.style.Style({
//		image: new ol.style.Circle({
//			radius: 5,
//			fill: new ol.style.Fill({
//			color: 'orange'
//			})
//		})
//	}));
//}

///centruje map� do przystanku!
function centerToStop(stopCoords) {
    var p = ol.proj.transform([stopCoords[0], stopCoords[1]], "EPSG:4326", "EPSG:3857");
    map.getView().animate({ zoom: 18 }, { center: p });
}

function createTextStyle(stopName) {
    return new ol.style.Text({
        text: stopName,
        stroke: new ol.style.Stroke({ color: [255, 255, 255], width: 10 }),
        font: 'bold 10px Verdana',
        offsetY: -25
    });
}

function getStopStyle(stopType, stopName) {
    if (stopType == "Tramwajowy" || stopType == "TramwajowoAutobusowy") {
        return new ol.style.Style({
            image: new ol.style.Icon({
                src: "/Images/ico_tram.png"
            }),
            text: createTextStyle(stopName)
        });
    }
    else if (stopType == "Autobusowy") {
        return new ol.style.Style({
            image: new ol.style.Icon({
                src: "/Images/ico_bus.png"
            }),
            text: createTextStyle(stopName)
        });
    }
    else if (stopType == "Kolejowy") {
        return new ol.style.Style({
            image: new ol.style.Icon({
                src: "/Images/ico_kolej.png"
            }),
            text: createTextStyle(stopName)
        });
    }
}

function getVehicleStyle(line) {

    var textStyle = new ol.style.Text({
     //   stroke: new ol.style.Stroke({ color: [255, 255, 255], width: 10 }),
        font: 'bold 12px Verdana',
        fill: new ol.style.Fill({ color: [0, 0, 0] }),
        text: line
    });

    return new ol.style.Style(
        {
            image: new ol.style.Circle({
                radius: 15,
                fill: new ol.style.Fill({
                    color: 'orange'
                })
            }),
            text: textStyle
        });
}

function displayVehiclesOnMap(vehicles) {

    vehiclesSource.clear();

    for (var i = 0; i < vehicles.length; i++) {
        var p = ol.proj.transform([vehicles[i].Lon, vehicles[i].Lat], "EPSG:4326", "EPSG:3857");
        console.log(p);
        var vehicleStyle = getVehicleStyle(vehicles[i].Lines);

        var vehicleFeature = new ol.Feature({
            geometry: new ol.geom.Point(p),
            lines: vehicles[i].Lines,
            brigade: vehicles[i].Brigade,
            time: vehicles[i].Time,
        });

        vehicleFeature.setStyle(vehicleStyle);
        var select = new ol.interaction.Select();
        map.addInteraction(select);
        select.on('select',
            function (e) {
                console.log(e);
                if (e.target.getFeatures().item(0) != null &&
                    typeof (e.target.getFeatures().item(0).get('group')) !== "undefined") {
                    openSingleStopPanel(e.target.getFeatures().item(0).get('group'),
                        e.target.getFeatures().item(0).get('post'));
                } else if (typeof (e.target.getFeatures().item(0).get('lines')) !== "undefined") {
                    openLinePanel(e.target.getFeatures().item(0).get('lines'));
                }
            });

        console.log(vehicleFeature);
        vehiclesSource.addFeature(vehicleFeature);
    }
}

var tileMapLayer = new ol.layer.Tile({ source: new ol.source.OSM() });

var stopsSource = new ol.source.Vector();

var stopsLayer = new ol.layer.Vector({
    source: stopsSource
});

var routesSource = new ol.source.Vector();

var routesLayer = new ol.layer.Vector({
    source: routesSource
});

var singleStopSource = new ol.source.Vector();

var singleStopLayer = new ol.layer.Vector({
    source: singleStopSource
});

var vehiclesSource = new ol.source.Vector();

var vehiclesLayer = new ol.layer.Vector({
    source: vehiclesSource
});

function displaySingleStop(stop) {
    clearRoutesOnMap();

    displayStops(stop, singleStopSource, false);

    centerToStop([stop[0].longitude, stop[0].latitude]);
}

function clearRoutesOnMap() {
    routesSource.clear();
    singleStopSource.clear();
}

function getLineStyle() {
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "#720050",
            width: 5
        })
    });
}

function displayRouteOnMap(points, stops) {
    routesSource.clear();

    if (points == null || points.length == 0)
        return;

    var centerTo = [0.0, 0.0];

    for (var i = 0; i < points.length; i++) {
        var p = ol.proj.transform([points[i].longitude, points[i].latitude], "EPSG:4326", "EPSG:3857");
        centerTo[0] += p[0];
        centerTo[1] += p[1];

        points[i] = p;
    }

    var lineFeature = new ol.Feature({
        geometry: new ol.geom.LineString(points),
        style: getLineStyle()
    });

    lineFeature.setStyle(getLineStyle());

    routesSource.addFeature(lineFeature);

    centerTo[0] = centerTo[0] / points.length;
    centerTo[1] = centerTo[1] / points.length;

    map.getView().fit(routesSource.getExtent(), { duration: 1000 });

    //map.getView().fit(routesSource.getExtent(), map.getSize(), { duration: 1000 });
    //map.getView().animate({ zoom: 13 }, { center: centerTo });

    displayStops(stops, routesSource, false);
}

function onMoveEnd(evt) {
    var map = evt.map;
    var extent = map.getView().calculateExtent(map.getSize());
    var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(extent),
        "EPSG:3857", "EPSG:4326");
    var topRight = ol.proj.transform(ol.extent.getTopRight(extent),
        "EPSG:3857", "EPSG:4326");

    console.log("left " + bottomLeft[0]);
    console.log("bottom " + bottomLeft[1]);
    console.log("right " + topRight[0]);
    console.log("top " + topRight[1]);

    $.ajax({
        url: "/Home/GetStopsInRegion",
        type: "get",
        data: {
            nwLat: topRight[1],
            nwLon: bottomLeft[0],
            seLat: bottomLeft[1],
            seLon: topRight[0],
        },
        success: function (response) {
            displayStops(response, stopsSource, true);
        },
        error: function (response) { console.warn("get stops on map error : " + response); }
    });
}

function displayStops(response, source, toClear) {

    if (toClear) { source.clear(); }

    var respLen = response.length;

    for (var i = 0; i < respLen; i++) {
        var p = ol.proj.transform([response[i].Longitude, response[i].Latitude], "EPSG:4326", "EPSG:3857");

        var feature = new ol.Feature({
            geometry: new ol.geom.Point(p),
            group: response[i].GroupNumber,
            post: response[i].PostNumber,
            name: response[i].Name
        });

        var select = new ol.interaction.Select();
        map.addInteraction(select);
        select.on('select',
            function (e) {
                console.log(e);
                if (e.target.getFeatures().item(0) != null &&
                    typeof (e.target.getFeatures().item(0).get('group')) !== "undefined") {
                    openSingleStopPanel(e.target.getFeatures().item(0).get('group'),
                        e.target.getFeatures().item(0).get('post'));
                } else if (typeof (e.target.getFeatures().item(0).get('lines')) !== "undefined") {
                    openLinePanel(e.target.getFeatures().item(0).get('lines'));
                }
                });

        feature.setStyle(getStopStyle(response[i].Type, response[i].Name));
        source.addFeature(feature);
    }
}

function createMap() {
    var mapRet = new ol.Map({
        layers: [tileMapLayer, stopsLayer, routesLayer, singleStopLayer, vehiclesLayer],
        target: "map",
        view: new ol.View({
            center: [2342583.9581056964, 6845966.78415203],
            zoom: 12
        }),
    });

    mapRet.on("moveend", onMoveEnd);



    return mapRet;
}