﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Routing;
using sip_waw.Entity;
using sip_waw.Entity.Entities;
using sip_waw.Models;

namespace sip_waw.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            //TextFileParser.TextFileParser parser = new TextFileParser.TextFileParser();
            //parser.AppDbContext = ctx;

            //parser.ParseText();

            //// ctx.DayTypes.Add(new Entity.Entities.DayType() { Description = "powszedni letni", Symbol = "DS" });

            //ctx.SaveChanges();

            return View();
        }

        public ActionResult GetLines(string searchQuery)
        {
            //if (string.IsNullOrWhiteSpace(searchQuery))
            //    return Json(null, JsonRequestBehavior.AllowGet);

            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var linesByType = dbContext.Lines.Where(x => x.Number.ToLower().StartsWith(searchQuery.ToLower()))
                    .ToLookup(x => x.Description, x => x.Number);

                if (!linesByType.Any())
                    linesByType = dbContext.Lines.ToLookup(x => x.Description, x => x.Number);

                Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>(linesByType.Count);

                foreach (var lt in linesByType)
                {
                    dict.Add(lt.Key, lt.ToList());
                }

                return Json(dict, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetStopGroups(string searchQuery)
        {
            if (string.IsNullOrWhiteSpace(searchQuery) || searchQuery.Length < 3)
                return Json(null, JsonRequestBehavior.AllowGet);

            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var stops = dbContext.StopGroups.Where(x => x.Name.ToLower().Contains(searchQuery.ToLower()) || x.Number == searchQuery).
                    Select(x => new
                    {
                        Number = x.Number,
                        Name = x.Name,
                        Town = x.Town.Name
                    }).ToList();
                return Json(stops, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetPostsDetails(string groupNumber)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var retList = dbContext.StopPosts.Where(x => x.StopGroupNumber == groupNumber)
                    .ToLookup(x => x.StopType, x => new
                    {
                        Name = x.StopGroup.Name + " " + x.PostNumber,
                        GroupNumber = x.StopGroupNumber,
                        PostNumber = x.PostNumber,
                        Street = x.StreetName,
                        Dest = x.CommonDestination,
                        Type = x.StopType.ToString(),
                        Longitude = x.Position.Longitude,
                        Latitude = x.Position.Latitude,
                    });
                var dict = retList.ToDictionary(x => x.Key.ToString(), x => x.ToList());

                return Json(dict, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetPost(string groupNumber, string postNumber)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var data = dbContext.StopPosts
                    .Where(x => x.StopGroupNumber == groupNumber && x.PostNumber == postNumber)
                    .Select(x => new
                    {
                        Name = x.StopGroup.Name + " " + x.PostNumber,
                        GroupNumber = x.StopGroupNumber,
                        PostNumber = x.PostNumber,
                        Street = x.StreetName,
                        Dest = x.CommonDestination,
                        Type = x.StopType.ToString(),
                        Longitude = x.Position.Longitude,
                        Latitude = x.Position.Latitude,
                        StopType = x.StopType
                    })
                    .First();


                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }




        public ActionResult GetPostLines(string groupNumber, string postNumber)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {


                var lines = dbContext.StopPosts
                        .First(x => x.PostNumber == postNumber && x.StopGroupNumber == groupNumber).StopPost_Lines
                        .ToLookup(d => d.StoppingCharacter, y => new
                        {
                            LineNumber = y.LineNumber,
                            Type = y.StoppingCharacter.ToString(),
                            Published = y.IsPublished
                        })
                            .ToDictionary(w => w.Key.ToString(), w => w.ToList());
                return Json(lines, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult GetNextDepartures(string groupNumber, string postNumber, int departuresCount = 7)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                var line_daytypes = dbContext.TimetableCalendars.Where(x => x.Date == today).ToList();

                Dictionary<string, string> line_days_dict = new Dictionary<string, string>(line_daytypes.Count);
                foreach (var entr in line_daytypes)
                {
                    line_days_dict.Add(entr.LineNumber, entr.DayTypeSymbol);
                }

                int timeNow = DateTime.Now.Hour * 60 + DateTime.Now.Minute;

                //&&
                //

                //var deps = dbContext.Departures.Where(x => x.PostNumber == post && x.StopGroupNumber == group).ToList().Take(5)
                //    .ToList();

                var deps = dbContext.Departures.Where(x =>
                        x.PostNumber == postNumber && x.StopGroupNumber == groupNumber && (60 * x.Hour + x.Minute) >= timeNow
                        && x.IsArrival == false)
                    .ToList()
                    .Where(x => line_days_dict.ContainsKey(x.LineNumber) && x.DayTypeSymbol == line_days_dict[x.LineNumber])
                    .OrderBy(x => (60 * x.Hour + x.Minute - timeNow)).Take(departuresCount);

                var depsToRet = deps.Select(x => new
                {
                    Course = x.CourseId,
                    LineNumber = x.LineNumber,
                    Hour = x.Hour,
                    Minute = x.Minute,
                    IsPublished = x.IsPublished,
                    IsLowFloor = x.IsLowFloor,
                    Destination = GetDestinationStop(x.LineNumber, x.CourseId)
                });

                return Json(depsToRet, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetDestinationStop(string lineNumber, string course)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var departure =
                    dbContext.Departures.FirstOrDefault(
                        x => x.LineNumber == lineNumber && x.CourseId == course && x.IsArrival);

                if (departure == null)
                    return string.Empty;

                var stop = dbContext.StopGroups.Find(departure.StopGroupNumber);

                return stop.Name;
            }
        }

        public ActionResult GetTimetableTable(string groupNumber, string postNumber, string lineNumber)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                string dayTypeSymbol = dbContext.TimetableCalendars.First(x => x.Date == today && x.LineNumber == lineNumber)
                    .DayTypeSymbol;
                string dayTypeName = dbContext.DayTypes.Find(dayTypeSymbol).Description;

                var departures = dbContext.Departures.Where(x => x.DayTypeSymbol == dayTypeSymbol
                                                                 && x.IsArrival == false
                                                                 && x.LineNumber == lineNumber
                                                                 && x.StopGroupNumber == groupNumber
                                                                 && x.PostNumber == postNumber)
                             .Select(x =>
                       new
                       {
                           Hour = x.Hour,
                           Minute = x.Minute,
                           Course = x.CourseId,
                           IsPublished = x.IsPublished,
                           IsLowFloor = x.IsLowFloor,
                           Legend = x.LegendSymbols
                       }).ToLookup(x => x.Hour, x => x)
                       .ToDictionary(x => x.Key.ToString(), x => x.ToList());

                Dictionary<string, object> d = new Dictionary<string, object>(2);
                d.Add("deps", departures);

                var legends = dbContext.TimetableLegends.Where(x =>
                    x.StopGroupNumber == groupNumber && x.StopPostNumber == postNumber && x.LineNumber == lineNumber).Select(x =>
                    new
                    {
                        Symbol = x.Symbol,
                        Description = x.Description
                    }).Distinct().ToList();

                d.Add("legends", legends);

                return Json(d, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetRouteTypes(string lineNumber)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var routeTypes = dbContext.RouteTypes.Where(x => x.LineNumber == lineNumber);
                var retList = routeTypes.Select(x => new
                {
                    Name = x.Name,
                    FirstStop = x.StopPost_RouteTypes.OrderBy(y => y.StopNumber).FirstOrDefault().StopPost.StopGroup.Name,
                    LastStop = x.StopPost_RouteTypes.OrderByDescending(y => y.StopNumber).FirstOrDefault().StopPost.StopGroup.Name,
                    Direction = x.RouteDirection.ToString(),
                    Level = x.RouteLevel
                }).ToList();

                ApiWarszawa.GetVehiclesForLine(lineNumber);

                return Json(retList, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetRouteStops(string lineNumber, string route)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                var d = dbContext.RouteTypes.Find(route, lineNumber);
                var c = d.StopPost_RouteTypes;
                var stopsList = dbContext.RouteTypes.Find(route, lineNumber).StopPost_RouteTypes.OrderBy(x => x.StopNumber)
                    .Select(x => new
                    {
                        Order = x.StopNumber,
                        Name = x.StopPost.StopGroup.Name + " " + x.PostNumber,
                        GroupNumber = x.PostGroupNumber,
                        PostNumber = x.PostNumber,
                        Type = x.StopPost.StopType.ToString(),
                        Longitude = x.StopPost.Position?.Longitude,
                        Latitude = x.StopPost.Position?.Latitude
                    }).ToList();

                return Json(stopsList, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetStopsInRegion(string nwLon, string nwLat, string seLon, string seLat)
        {
            //  try
            //  {

            using (ApplicationDbContext dbContext = new ApplicationDbContext())
            {
                double nwLongitude = double.Parse(nwLon, CultureInfo.InvariantCulture);
                double nwLatitude = double.Parse(nwLat, CultureInfo.InvariantCulture);

                double seLongitude = double.Parse(seLon, CultureInfo.InvariantCulture);
                double seLatitude = double.Parse(seLat, CultureInfo.InvariantCulture);

                DbGeography region = DbGeography.FromText(
                    string.Format("POLYGON(({0} {1}, {0} {2}, {3} {2}, {3} {1}, {0} {1}))",
                        nwLongitude.ToString(CultureInfo.InvariantCulture),
                        nwLatitude.ToString(CultureInfo.InvariantCulture),
                        seLatitude.ToString(CultureInfo.InvariantCulture),
                        seLongitude.ToString(CultureInfo.InvariantCulture)), 4326);

                var postsInRegion = dbContext.StopPosts.Where(x => region.Intersects(x.Position)).Select(x => new
                {
                    Name = x.StopGroup.Name + " " + x.PostNumber,
                    Latitude = x.Position.Latitude,
                    Longitude = x.Position.Longitude,

                    GroupNumber = x.StopGroupNumber,
                    PostNumber = x.PostNumber,

                    Type = x.StopType.ToString()
                }).ToList();

                if (postsInRegion.Count > 150)
                    postsInRegion.Clear();

                return Json(postsInRegion, JsonRequestBehavior.AllowGet);
            }


            //  }
            //   catch (Exception e)
            //   {
            //
            //  }
        }

        public ActionResult GetAllVehicles()
        {
            return Json(ApiWarszawa.GetAllVehicles(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehiclesForLine(string line)
        {
            return Json(ApiWarszawa.GetVehiclesForLine(line), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehiclesForStop(string group, string post)
        {
            return Json(ApiWarszawa.GetVehiclesForStop(group, post), JsonRequestBehavior.AllowGet);
        }
    }
}