﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace sip_waw.App_Start
{

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                  .Include("~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/css")
                        .Include("~/Content/bootstrap.css"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Index/pojazdy_styl.css"));

            bundles.Add(new ScriptBundle("~/bundles/AppScripts").Include(
                      "~/Scripts/Index/control_panel.js",
                      "~/Scripts/Index/create_map.js"));
        }
    }
}
