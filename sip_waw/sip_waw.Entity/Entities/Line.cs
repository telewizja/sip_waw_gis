﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class Line
    {
        [Key, StringLength(4)]
        public string Number { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public virtual ICollection<RouteType> RouteTypes { get; set; }
    }
}
