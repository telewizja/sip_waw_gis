﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using sip_waw.Entity.Enums;

namespace sip_waw.Entity.Entities
{
    public class StopPost_Line
    {
        [Key, Column(Order = 3)]
        public string LineNumber { get; set; }

        [Key, Column(Order = 1)]
        public string PostGroupNumber { get; set; }

        [Key, Column(Order = 2)]
        public string PostNumber { get; set; }

        [Key, Column(Order = 4)]
        public StoppingCharacter StoppingCharacter { get; set; }

        public bool IsPublished { get; set; }

        /// <summary>
        /// Zapomnieliśmy o zbiorczym opisie dla rozkłądu jazdy linii z przystanku
        /// </summary>
        public string LegendDescription { get; set; }

        /// <summary>
        /// Rozkład ważny od... - zawsze jest taki wpis w legendzie
        /// </summary>
        public DateTime TimetableValidFrom { get; set; }

        [ForeignKey("LineNumber")]
        public virtual Line Line { get; set; }

        [ForeignKey("PostGroupNumber, PostNumber")]
        public virtual StopPost StopPost { get; set; }
    }
}
