﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class TimetableCalendar
    { 
        [Key, Column(Order=1)]
        public DateTime Date { get; set; }

        [Key, Column(Order = 2), StringLength(4)]
        public string LineNumber { get; set; }

        [Key, Column(Order = 3), StringLength(2, MinimumLength = 2)]
        public string DayTypeSymbol { get; set; }

        [ForeignKey("DayTypeSymbol")]
        public virtual DayType DayType { get; set; }

        [ForeignKey("LineNumber")]
        public virtual Line Line { get; set; }
    }
}
