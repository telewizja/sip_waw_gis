﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class StopPost_RouteType
    {
        [Key, Column(Order = 1), StringLength(4, MinimumLength = 4)]
        public string PostGroupNumber { get; set; }

        [Key, Column(Order = 2), StringLength(2, MinimumLength = 2)]
        public string PostNumber { get; set; }

        [Key, Column(Order = 3), StringLength(50)]
        public string RouteTypeName { get; set; }

        [Key, Column(Order = 4), StringLength(4)]
        public string LineNumber { get; set; }

        [Key, Column(Order = 5)]
        public int StopNumber { get; set; }

        [ForeignKey("RouteTypeName, LineNumber")]
        public virtual RouteType RouteType { get; set; }

        [ForeignKey("PostGroupNumber, PostNumber")]
        public virtual StopPost StopPost { get; set; }
    }
}
