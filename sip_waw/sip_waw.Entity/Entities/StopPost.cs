﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Spatial;
using sip_waw.Entity.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace sip_waw.Entity.Entities
{
    public class StopPost
    {
        [Key, Column(Order = 1), StringLength(4)]
        public string StopGroupNumber { get; set; }

        [Key, Column(Order = 2), StringLength(2)]
        public string PostNumber { get; set; }

        [StringLength(30)]
        public string StreetName { get; set; }

        [StringLength(30)]
        public string CommonDestination { get; set; }

        public StopType StopType { get; set; }

        public DbGeography Position { get; set; }

        [ForeignKey("StopGroupNumber")]
        public virtual StopGroup StopGroup { get; set; }

        public virtual ICollection<StopPost_Line> StopPost_Lines { get; set; }
    }
}
