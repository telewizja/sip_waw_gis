﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class StopGroup
    {
        [Key, StringLength(4)]
        public string Number { get; set; }

        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(2, MinimumLength = 2)]
        public string TownCode { get; set; }

        [ForeignKey("TownCode")]
        public virtual Town Town { get; set; }

        public virtual ICollection<StopPost> StopPosts { get; set; }
    }
}
