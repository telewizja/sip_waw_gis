﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class TimetableLegend
    {
        private static int _id;

        public TimetableLegend() { _id++; Id = _id; }

        [Key]
        public int Id { get; set; }

        [StringLength(4, MinimumLength = 4)]
        public string StopGroupNumber{ get; set; }

        [StringLength(2, MinimumLength = 2)]
        public string StopPostNumber { get; set; }

        [StringLength(5)]
        public string LineNumber { get; set; }

        [StringLength(5)]
        public string Symbol { get; set; }

        public string Description { get; set; }
    }
}
