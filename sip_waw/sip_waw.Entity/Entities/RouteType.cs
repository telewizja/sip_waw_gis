﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using sip_waw.Entity.Enums;

namespace sip_waw.Entity.Entities
{
    public class RouteType
    {
        [Key, Column(Order=1), StringLength(50)]
        public string Name { get; set; }

        [Key, Column(Order=2), StringLength(4)]
        public string LineNumber { get; set; }

        public RouteDirection RouteDirection { get; set; }

        public int RouteLevel { get; set; }

        [ForeignKey("LineNumber")]
        public virtual Line Line { get; set; }

        public virtual ICollection<StopPost_RouteType> StopPost_RouteTypes { get; set; }
    }
}
