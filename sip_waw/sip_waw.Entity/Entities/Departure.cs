﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class Departure
    {
        private static int _id;

        public Departure() { _id++; Id = _id; }

        [Key]
        public int Id { get; set; }

        [StringLength(17, MinimumLength = 17)]
        public string CourseId { get; set; }

        [StringLength(4)]
        public string LineNumber { get; set; }

        [StringLength(4, MinimumLength = 4)]
        public string StopGroupNumber { get; set; }

        [StringLength(2, MinimumLength = 2)]
        public string PostNumber { get; set; }

        [StringLength(5)]
        public string BrigadeNumber { get; set; }

        public int Hour { get; set; }

        public int Minute { get; set; }

        [StringLength(2, MinimumLength = 2)]
        public string DayTypeSymbol { get; set; }

        public bool IsPublished { get; set; }

        public bool IsArrival { get; set; }

        public bool IsLowFloor { get; set; }

        public string LegendSymbols { get; set; } = string.Empty;

        //[ForeignKey("LineNumber, BrigadeNumber, CourseId")]
        //public virtual BrigadeLine BrigadeLine { get; set; }

        //[ForeignKey("StopGroupNumber, PostNumber")]
        //public virtual StopPost StopPost { get; set; }

        //[ForeignKey("DayTypeSymbol")]
        //public virtual DayType DayType { get; set; }
    }
}
