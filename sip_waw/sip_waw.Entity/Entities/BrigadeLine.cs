﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class BrigadeLine
    {
        [Key, Column(Order = 1), StringLength(4)]
        public string LineNumber { get; set; }

        [Key, Column(Order = 2), StringLength(5)]
        public string BrigadeNumber { get; set; }

        [Key, Column(Order = 3), StringLength(10)]
        public string CourseId { get; set; }

        //[ForeignKey("LineNumber")]
        //public virtual Line Line { get; set; }
    }
}
