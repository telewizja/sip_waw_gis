﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Entities
{
    public class DayType
    {
        [Key, StringLength(2, MinimumLength = 2)]
        public string Symbol { get; set; }

        [StringLength(50)]
        public string Description { get; set; }
    }
}
