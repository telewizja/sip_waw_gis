﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sip_waw.Entity.Enums
{
    public enum StoppingCharacter
    {
        Stały, Warunkowy, Dla_wsiadających, Dla_wysiadających, Krańcowy, Postojowy
    }
}
