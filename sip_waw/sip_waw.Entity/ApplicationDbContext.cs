﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sip_waw.Entity.Entities;

namespace sip_waw.Entity
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {

        }

        public virtual DbSet<BrigadeLine> BrigadeLines { get; set; }

        public virtual DbSet<DayType> DayTypes { get; set; }

        public virtual DbSet<Departure> Departures { get; set; }

        public virtual DbSet<Line> Lines { get; set; }

        public virtual DbSet<RouteType> RouteTypes { get; set; }

        public virtual DbSet<StopGroup> StopGroups { get; set; }

        public virtual DbSet<StopPost_Line> StopPost_Lines { get; set; }

        public virtual DbSet<StopPost_RouteType> StopPost_RouteTypes { get; set; }

        public virtual DbSet<StopPost> StopPosts { get; set; }

        public virtual DbSet<TimetableCalendar> TimetableCalendars { get; set; }

        public virtual DbSet<TimetableLegend> TimetableLegends { get; set; }

        public virtual DbSet<Town> Towns { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToOneConstraintIntroductionConvention>();

        }

    }
}
